﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class disciplina : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pegarDisciplinas();
            }
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            Disciplina dis = new Disciplina();
            DisciplinaDAO disDAO = new DisciplinaDAO();
            if (txtNomeDisciplina.Text != "" || txtCodigoDisciplina.Text != "" || txtCargaHorariaDisciplina.Text != "")
            {
                dis.nome = txtNomeDisciplina.Text;
                dis.codigo = txtCodigoDisciplina.Text;
                dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                disDAO.Cadastrar(dis);

                msg.InnerText = "Cadastrado com sucesso!!!";
            }
            else
            {

                msg.InnerText = "Campo não preencido";
            }
            limpar();
        }

        public void pegarDisciplinas()
        {
            DisciplinaDAO dd = new DisciplinaDAO();
            ddlId.DataSource = dd.ListarDisciplinas();
            ddlId.DataValueField = "idDisciplina";
            ddlId.DataTextField = "nome";
            ddlId.DataBind();
        }

        public void limpar()
        {
            txtNomeDisciplina.Text = string.Empty;
            txtCodigoDisciplina.Text = string.Empty;
            txtCargaHorariaDisciplina.Text = string.Empty;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            DisciplinaDAO dd = new DisciplinaDAO();
            Disciplina d = dd.BuscarDisciplina(Convert.ToInt32(ddlId.SelectedValue));
            txtNomeDisciplina.Text = d.nome;
            txtCodigoDisciplina.Text = d.codigo;
            txtCargaHorariaDisciplina.Text = Convert.ToString(d.cargaHoraria);
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            Disciplina dis = new Disciplina();
            DisciplinaDAO disDAO = new DisciplinaDAO();

            if (txtNomeDisciplina.Text != "" || txtCodigoDisciplina.Text != "" || txtCargaHorariaDisciplina.Text != "")
            {
                dis.idDisciplina = Convert.ToInt32(ddlId.SelectedValue);
                dis.nome = txtNomeDisciplina.Text;
                dis.codigo = txtCodigoDisciplina.Text;
                dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                disDAO.Atualizar(dis);

                msg.InnerText = "Alterado com sucesso!!!";
            }
            else
            {
                msg.InnerText = "Campo não preencido";
            }
            limpar();
        }

        protected void btnListar_Click(object sender, EventArgs e)
        {
            DisciplinaDAO dd = new DisciplinaDAO();
            grdDisciplinas.DataSource = dd.ListarDisciplinas();
            grdDisciplinas.DataBind();
            pegarDisciplinas();
            limpar();
        }

        protected void btnDeletar_Click(object sender, EventArgs e)
        {
            DisciplinaDAO dd = new DisciplinaDAO();
            Disciplina d = new Disciplina();
            d.idDisciplina = Convert.ToInt32(ddlId.SelectedValue);
            dd.Deletar(d);
            pegarDisciplinas();
        }

      
    }
}