﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="altear_disciplina.aspx.cs" Inherits="geducSite.View.altear_disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
        <div class="box-content">
            <form id="form1" runat="server">
                <fieldset>
                    <legend class="text-center">Disciplina</legend>
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblCodigoDisciplina" runat="server" Text="Codigo Disciplina:"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtCodigoDisciplina" runat="server"></asp:TextBox>
                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-primary btn-xs" OnClick="btnBuscar_Click" Text="Preencher automaticamente" />

                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblNomeDisciplina" runat="server" Text="Nome Disciplina:"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtNomeDisciplina" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblCargaHorariaDisciplina" runat="server" Text="Carga Horaria Disciplina:"></asp:Label>

                        <div class="col-sm-10">
                            <asp:TextBox ID="txtCargaHorariaDisciplina" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <br />
                </fieldset>
                <br />
                <p>
                    <span id="msg" runat="server">&nbsp;</span>
                </p>
                <br />
                <asp:Button ID="btnCadastrar" CssClass="btn btn-primary btn-lg" runat="server" Text="Alterar" OnClick="btnAlterar_Click" />
            </form>
        </div>
    </div>
</asp:Content>
