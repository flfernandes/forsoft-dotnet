﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Printing;

namespace geducSite.View
{
    public partial class altear_disciplina : System.Web.UI.Page
    {

        protected IEnumerable<Disciplina> todasAsDisciplina;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                todasAsDisciplina = (IEnumerable<Disciplina>)Session["todasAsDisciplina"];
            }
            else
            {
                todasAsDisciplina = new DisciplinaDAO().ListarDisciplinas();
                Session["todasAsDisciplina"] = todasAsDisciplina;
                Session.Timeout = 6000;
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            msg.InnerText = "";
            String[] campos = new String[] { txtNomeDisciplina.Text, txtCodigoDisciplina.Text, txtCargaHorariaDisciplina.Text };
            if (Validar.seSomenteNumero(txtCargaHorariaDisciplina.Text))
            {
                if (!Validador.seTudoVazio(campos))
                {
                    Disciplina dis = todasAsDisciplina.SingleOrDefault(x => x.codigo == txtCodigoDisciplina.Text);
                    if (dis != null)
                    {
                        if (!Validar.seVazio(txtNomeDisciplina.Text))
                            dis.nome = txtNomeDisciplina.Text;
                        if (!Validar.seVazio(txtCodigoDisciplina.Text))
                            dis.codigo = txtCodigoDisciplina.Text;
                        if (!Validar.seVazio(txtCargaHorariaDisciplina.Text))
                            dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                        new DisciplinaDAO().Atualizar(dis);

                        msg.InnerText = "Alterado com sucesso!!!";
                        limpar();
                    }
                    else msg.InnerText = "Disciplina não encontrada";
                }
                else
                {
                    msg.InnerText = "Nenhum campo preenchido";
                }
            }
            else msg.InnerText = "Preenchimento de campo inválido";
        }

        public void limpar()
        {
            txtNomeDisciplina.Text = string.Empty;
            txtCodigoDisciplina.Text = string.Empty;
            txtCargaHorariaDisciplina.Text = string.Empty;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Disciplina d = todasAsDisciplina.SingleOrDefault(x => x.codigo == txtCodigoDisciplina.Text);
            if (d != null)
            {
                txtNomeDisciplina.Text = d.nome;
                txtCargaHorariaDisciplina.Text = Convert.ToString(d.cargaHoraria);
            }
        }
    }
}